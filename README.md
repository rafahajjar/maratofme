# Web de la marató de problemes de la FME 2020

Per tal de generar el web en local simplement cal executar el programa
`genera-web.py` i es generarà el web dins la carpeta `public`.

Les dades es generen a partir del fitxer `dades.json` que té la següent forma
(equips és un vector d'objectes de l'estil que hi ha aquí com a exemple):

```json
{
  "equips": [
    {
      "nom": "",
      "integrants": [
        "",
        "",
        ""
      ],
      "puntuacions": {
        "l1": {"p1": "", "p2": "", "p3": "", "p4": "", "p5": "", "p6": ""},
        "l2": {"p1": "", "p2": "", "p3": "", "p4": "", "p5": "", "p6": ""},
        "l3": {"p1": "", "p2": "", "p3": "", "p4": "", "p5": "", "p6": ""},
        "l4": {"p1": "", "p2": "", "p3": "", "p4": "", "p5": "", "p6": ""},
        "l5": {"p1": "", "p2": "", "p3": "", "p4": "", "p5": "", "p6": ""},
        "l6": {"p1": "", "p2": "", "p3": "", "p4": "", "p5": "", "p6": ""},
        "l7": {"p1": "", "p2": "", "p3": "", "p4": "", "p5": "", "p6": ""}
      }
    }
  ]
}
```

Si els fitxers es posen amb el nom correcte i al directori correcte, el web es
generarà automàticament. L'estructura de fitxers ha de quedar així:

```
.
├── dades.json
├── genera-web.py
├── index.html
├── llistes
│   ├── llista-1.pdf
│   ├── llista-2.pdf
│   ├── llista-3.pdf
│   └── llista-4.pdf
├── README.md
└── resolucions
    ├── llista-1
    │   ├── p1-1.pdf
    │   ├── p2-4.pdf
    │   ├── p3-13.pdf
    │   ├── p4-28.pdf
    │   ├── p5-58.pdf
    │   └── p6-31.pdf
    └── llista-2
        ├── p1-64.pdf
        ├── p2-130.pdf
        ├── p3-55.pdf
        ├── p4-112.pdf
        ├── p5-226.pdf
        └── p6-454.pdf
```
